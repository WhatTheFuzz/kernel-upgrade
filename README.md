# Upgrading the Linux Kernel

## Introduction

I made this short how-to because other guides failed me. I was attempting to
update my virtual machine tools on my guest when I found that I had installed
5.11 Linux headers but was running a 5.4 kernel. The VM tools installation was
basing dependency installation on my kernel version, but the 5.4 headers were
not present on the latest Aptitude repository. So here we are.

* `wget` the latest image, headers, and modules from the [Ubuntu kernel
releases][kernel] site. In this case, I am grabbing the latest 5.13 release for
arm. Make sure to get the correct architecture for your machine!

```shell
cd /tmp
$ wget
https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.13.9/amd64/linux-headers-5.13.9-051309_5.13.9-051309.202108080438_all.deb
$ wget
https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.13.9/arm64/linux-headers-5.13.9-051309-generic_5.13.9-051309.202108080438_arm64.deb
$ wget
https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.13.9/arm64/linux-image-unsigned-5.13.9-051309-generic_5.13.9-051309.202108080438_arm64.deb
$ wget
https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.13.9/arm64/linux-modules-5.13.9-051309-generic_5.13.9-051309.202108080438_arm64.deb
```

* Install the packages

```shell
apt install ./linux*
```

* The step that I found most guides were missing: update grub.

```shell
$ sudo apt install grub2-common
$ sudo update-grub
Sourcing file `/etc/default/grub'
Sourcing file `/etc/default/grub.d/init-select.cfg'
Generating grub configuration file ...
Found linux image: /boot/vmlinuz-5.13.9-051309-generic
Found initrd image: /boot/initrd.img-5.13.9-051309-generic
...
```

* Reboot and you should be good to go! Confirm with:

```shell
$ uname --kernel-release --kernel-name
Linux 5.13.9-051309-generic
```

[kernel]: https://kernel.ubuntu.com/~kernel-ppa/mainline/?C=N;O=D
